﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Threading;
using System.Windows;

namespace Long_Knight_II
{    
    /// <summary>
    /// Updates current time. I DID NOT WRITE THIS CLASS. Stolen from http://social.msdn.microsoft.com/Forums/en-US/wpf/thread/1d7a8305-d2f1-46c6-bc5c-4a68d05f21d7/ .
    /// </summary>
    public class autosaveClass : INotifyPropertyChanged
    {
        /// <summary>
        /// Fires when time to autosave.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        private string _now;

        /// <summary>
        /// Constructor for class.
        /// </summary>
        public autosaveClass()
        {
            _now = "Welcome to Long Knight II";
            DispatcherTimer timer = new DispatcherTimer();
            timer.Interval = TimeSpan.FromSeconds(15);
            timer.Tick += new EventHandler(timer_Tick);
            timer.Start();
        }

        /// <summary>
        /// The current time.
        /// </summary>
        public string Now
        {
            get { return _now; }
            private set
            {
                _now = ("Last saved at: " + value);

                if (PropertyChanged != null)
                {
                    Engine.WriteData();
                    PropertyChanged(this, new PropertyChangedEventArgs("Now"));
                }
            }

        }

        void timer_Tick(object sender, EventArgs e)
        {
            Now = DateTime.Now.ToString("hh:mm:ss tt");
        }
    }
}

