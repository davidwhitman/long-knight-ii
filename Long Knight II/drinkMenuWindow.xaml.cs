﻿using System;
using System.Windows;
using System.Windows.Documents;

namespace Long_Knight_II
{
    /// <summary>
    /// Interaction logic for drinkMenuWindow.xaml
    /// </summary>
    public partial class drinkMenuWindow : Window
    {
        /// <summary>
        /// Initializes the window components.
        /// </summary>
        public drinkMenuWindow()
        {
            InitializeComponent();
            TextRange range = new TextRange(textbox.Document.ContentStart, textbox.Document.ContentEnd);
            Engine.ReadDrinks(ref range);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Owner.IsEnabled = true;
            Owner.Activate();
        }

        //Submit button
        private void Button_Click(object sender, RoutedEventArgs e) 
        {
            Engine.WriteDrinks(new TextRange(textbox.Document.ContentStart, textbox.Document.ContentEnd));
            Close();
        }

        //Cancel button
        private void Button_Click_1(object sender, RoutedEventArgs e) 
        {
            Close();
        }
    }
}
