﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Documents;

namespace Long_Knight_II
{
    /// <summary>
    /// Performs read/write operations.
    /// </summary>
    static class Engine
    {
        static string FOLDER_PATH = (Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)) + "\\Long Knight II";
        static string DATAPATH = FOLDER_PATH + "\\LKData.bin";
        static string LOGPATH = FOLDER_PATH + "\\LKlog.txt";
        static string DRINKPATH = FOLDER_PATH + "\\drinks.txt";

        static Engine()
        {
            if (Data.DEBUG)
            {
                OpenWindow(Data.output);
            }
        }

        /// <summary>
        /// Opens a new window.
        /// </summary>
        /// <param name="child">Window to be opened.</param>
        public static void OpenWindow(Window child)
        {
            //child.Topmost = true;
            child.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            child.Show();
        }

        /// <summary>
        /// Opens a new window and sets its owner.
        /// </summary>
        /// <param name="child">Window to be opened.</param>
        /// <param name="parent">Parent window (use 'this' keyword).</param>
        public static void OpenWindow(Window child, Window parent)
        {
            child.Topmost = true;
            child.Owner = parent;
            child.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            child.Show();
            parent.IsEnabled = false;
        }

        /// <summary>
        /// Writes errors to console only. (reference version)
        /// </summary>
        /// <param name="message"></param>
        private static void ConsoleLog(ref string message)
        {
            var str = DateTime.Now.ToString();
            str += "\t" + message + "\n";
            message = str;
            Data.output.log(message);
        }

        /// <summary>
        /// Writes errors to console only.
        /// </summary>
        /// <param name="message"></param>
        public static void LogConsole(string message)
        {
            var str = DateTime.Now.ToString();
            str += "\t" + message + "\n";
            Data.output.log(str);
        }

        /// <summary>
        /// Writes text to file and the output window.
        /// </summary>
        /// <param name="message">Message string.</param>
        public static void Log(string message)
        {
            var str = message;
            ConsoleLog(ref str); //now str is properly formatted with date

            try
            {
                var sw = new StreamWriter(LOGPATH, true);
                sw.Write(str);
                sw.Close();
            }
            catch (Exception e)
            {
                LogConsole(e.Message); //don't need to use by ref version, probably can't
            }
        }

        /// <summary>
        /// Loads drinks.txt into the TextRange parameter.
        /// </summary>
        /// <param name="range">TextRange to be loaded with text.</param>
        public static void ReadDrinks(ref TextRange range)
        {
            try
            {
                var fStream = new FileStream(DRINKPATH, FileMode.OpenOrCreate);
                range.Load(fStream, System.Windows.DataFormats.Text);
                fStream.Close();
            }
            catch (Exception e)
            {
                Log(e.Message);
            }
        }

        /// <summary>
        /// Returns drinks.txt.
        /// </summary>
        public static string ReadDrinks()
        {
            if (!File.Exists(DRINKPATH))
            {
                return "";
            }

            try
            {
                var fStream = new StreamReader(DRINKPATH);
                var temp = "";
                while (!fStream.EndOfStream)
                {
                    temp += fStream.ReadLine();
                    temp += '\n';
                }
                fStream.Close();
                return temp;
            }
            catch (Exception e)
            {
                Log(e.Message);
                return "";
            }
        }

        /// <summary>
        /// Writes the TextRange parameter to a file named drinks.txt.
        /// </summary>
        /// <param name="range">The TextRange to be written.</param>
        public static void WriteDrinks(TextRange range)
        {
            try
            {
                var fs = new FileStream(DRINKPATH, FileMode.Create);
                range.Save(fs, DataFormats.Text);
                fs.Close();
            }
            catch (Exception e)
            {
                Log(e.Message);
            }
        }

        public static void WriteData()
        {
            try
            {
                Stream stream = File.Open(DATAPATH, FileMode.Create);
                var bf = new BinaryFormatter();
                foreach (var p in Data.getData())
                {
                    bf.Serialize(stream, p);
                }
                stream.Close();
            }
            catch (Exception e)
            {
                Log(e.Message);
                return;
            }

        }

        public static void LoadData()
        {
            if (!File.Exists(DATAPATH))
            {
                LogConsole("Could not load LKData.bin. Creating file.");
            }
            try
            {
                Stream stream = File.Open(DATAPATH, FileMode.OpenOrCreate);
                var br = new BinaryFormatter();
                for (var i = 0; stream.Position < stream.Length; ++i)
                {
                    Data.Add((Person)br.Deserialize(stream));
                }
                stream.Close();
            }
            catch (Exception e)
            {
                Log(e.Message);
                return;
            }
        }
    }
}
