﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows;

namespace Long_Knight_II
{
    /// <summary>
    /// The Model (data) of the program. If any other part of the program needs data, it should be stored here.
    /// </summary>
    static class Data
    {
        /// <summary>
        /// The version number of the program.
        /// </summary>
        public const double VERSION = .1; //<------------VERSION NUMBER

        /// <summary>
        /// Turn debugmode on or off.
        /// </summary>
        public const bool DEBUG = true;

        /// <summary>
        /// The list of Persons that is shown in the ListView
        /// </summary>
        public static List<Person> Datas = new List<Person>();

        /// <summary>
        /// The error output window, visible only in debug mode.
        /// </summary>
        public static OutputWindow output = new OutputWindow();

        /// <summary>
        /// Whether clicking on a row will bring up the edit window.
        /// </summary>
        public static bool EDITMODE = false;

        /// <summary>
        /// Whether clicking on a row will delete that row.
        /// </summary>
        public static bool DELETEMODE = false;

        public static string THEME;


        /// <summary>
        /// Add a person to the end of the list.
        /// </summary>
        /// <param name="per"></param>
        public static void Add(Person per)
        {
            Datas.Add(per);
        }

        /// <summary>
        /// Returns the List of Persons.
        /// </summary>
        /// <returns>List of type Person with all Persons.</returns>
        public static List<Person> getData()
        {
            return Datas;
        }

        /// <summary>
        /// Returns a specific Person.
        /// </summary>
        /// <param name="i"></param>
        /// <returns>The Person at the specified index.</returns>
        public static Person getPerson(int i)
        {
            return Datas[i];
        }

        /// <summary>
        /// Removes element at position i.
        /// </summary>
        /// <param name="i">Position of element to remove.</param>
        public static void erase(int i)
        {
            Datas.RemoveAt(i);
        }

        public delegate void fireDataChangeHandler(); //Observed
        public static event fireDataChangeHandler dataChange;

        /// <summary>
        /// Call to notify all observers that the Data has been changed.
        /// </summary>
        public static void fireDataChange() //Call to update listview
        {
            dataChange();
        }
    }
}
