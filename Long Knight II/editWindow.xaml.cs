﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace Long_Knight_II
{
    /// <summary>
    /// Interaction logic for editWindow.xaml
    /// </summary>
    public partial class editWindow : Window
    {
        int i;
        public editWindow(int index)
        {
            i = index;
            InitializeComponent();

            comboLoad();
            nameBox.Text = Data.getPerson(index).name;
            dropdown.SelectedIndex = (dropdown.Items.IndexOf(Data.getPerson(index).type));
            numberboxleft.Text = Data.getPerson(index).left.ToString();
            numberboxdrank.Text = Data.getPerson(index).drank.ToString();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Owner.IsEnabled = true;
            Owner.Activate();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Data.getPerson(i).edit(nameBox.Text, dropdown.SelectedItem.ToString(), int.Parse(numberboxleft.Text), int.Parse(numberboxdrank.Text));
            }
            catch (Exception ee)
            {
                Engine.Log(ee.Message);
                return;
            }
            Data.fireDataChange();
            Close();
        }

        //Edit Button
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            Window drinks = new drinkMenuWindow();
            Engine.OpenWindow(drinks, this);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e) //cancel, automatically calls window_closed
        {
            Close();
        }

        private void comboLoad()
        {
            dropdown.Items.Clear();
            string[] str = Engine.ReadDrinks().Split('\n');
            for (int i = 0; i < str.Length-1; ++i)
            {
                dropdown.Items.Add(str[i]);
            }
        }

        private void Window_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e) //When it gains or loses focus, so it reloads combobox
        {
            comboLoad();
        }
    }
}

