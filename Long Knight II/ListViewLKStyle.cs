﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Long_Knight_II
{
    /// <summary>
    /// Custom ListView with highlighting.
    /// </summary>
    public class ListViewLKStyle : ListView
    {
        /// <summary>
        /// Every other row is beige. Other highlighting may apply.
        /// </summary>
        /// <param name="element"></param>
        /// <param name="item"></param>
        protected override void PrepareContainerForItemOverride(DependencyObject element, object item)
        {
            base.PrepareContainerForItemOverride(element, item);
            if (View is GridView)
            {
                int index = ItemContainerGenerator.IndexFromContainer(element);
                ListViewItem lvi = element as ListViewItem;
                Person p = item as Person;
                if (index % 2 == 0)
                {
                    lvi.Background = Brushes.Beige;
                }
                if (p.left == 0)
                {
                    lvi.Background = Brushes.Red;
                }
                else if (p.left <= 2)
                {
                    lvi.Background = Brushes.Pink;
                }
            }
        }
    }
}