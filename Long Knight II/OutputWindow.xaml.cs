﻿using System;
using System.Windows;

namespace Long_Knight_II
{
    /// <summary>
    /// Interaction logic for OutputWindow.xaml
    /// </summary>
    public partial class OutputWindow : Window
    {
        /// <summary>
        /// Initializes the window components.
        /// </summary>
        public OutputWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Writes a message to the screen.
        /// </summary>
        /// <param name="message">Message to be printed.</param>
        public void log(string message)
        {
            OutputBox.Text += message;
            scroller.ScrollToBottom();
        }
    }
}
