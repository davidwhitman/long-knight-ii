﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.IO;
using System.Windows.Media.Imaging;

namespace Long_Knight_II
{
  /// <summary>
  /// The root window the user sees with all of the buttons,the ListView, and the menus. Subclass of Window.
  /// </summary>
  public partial class MainWindow : Window
  {

    /// <summary>
    /// Gets the main data.
    /// </summary>
    /// <value>The main data.</value>
    public List<Person> MainData { get { return Data.Datas; } set { Data.Datas = value; } }

    /// <summary>
    /// Default constructor of MainWindow. Initializes xaml. Adds observer of Data.
    /// </summary>
    public MainWindow()
    {
      InitializeComponent();
      Engine.Log("Program opened.");
      Engine.LoadData();

      string pwd = Directory.GetCurrentDirectory();
      //Image img = new Image();
      //img.Source = new BitmapImage(new Uri(pwd + @"\\Resources\\ThemeLight\\newPersonButton.png"));
      newPersonButton.Background = new ImageBrush(new BitmapImage(new Uri(pwd + @"\\Resources\\ThemeLight\\newPersonButton.png")));
      //newPersonButton
      //exitButton.Content = new BitmapImage(new Uri(pwd + @"\\Resources\\ThemeLight\\exitButton.png"));

      foreach (Person person in Data.getData())
      {
        maingrid.Items.Add(person);
      }

      Data.dataChange += refresh; //Add observer
    }
    /// <summary>
    /// If Data.fireDataChange is fired, this will see that and reload data in the ListView.
    /// </summary>
    public void refresh()
    {
      maingrid.Items.Clear();
      foreach (Person person in Data.getData())
      {
        maingrid.Items.Add(person);
      }
    }

    /// <summary>
    /// Sets default window title when window is loaded to Long Knight II vx.xx .
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Window_Loaded(object sender, RoutedEventArgs e)
    {
      Title = "Long Knight II v" + Data.VERSION;
    }

    /// <summary>
    /// When the users clicks 'Quit', close the window. This will automatically call window_closing
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Quit_Click(object sender, RoutedEventArgs e)
    {
      Close();
    }

    /// <summary>
    /// When the users clicks New Person, bring up that window and disable this one.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void new_person_click(object sender, RoutedEventArgs e) //New Person
    {
      Engine.OpenWindow(new newPersonWindow(), this);
    }

    /// <summary>
    /// Handles click on Open button.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void Button_Click(object sender, RoutedEventArgs e) //Open
    {
      try
      {
        ((Person)maingrid.SelectedItems[0]).name = "testing";
        MessageBox.Show(((Person)(maingrid.Items[1])).ToString());
      }
      catch
      {
      }
    }

    /// <summary>
    /// Handles a left click on the listview. Took a drink.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MaingridMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
    {
      if ((maingrid.SelectedIndex >= maingrid.Items.Count) || (maingrid.SelectedIndex < 0))
        return;
      if (Data.EDITMODE)
      {
        Engine.OpenWindow(new editWindow(maingrid.SelectedIndex), this);
        return;
      }
      if (Data.DELETEMODE)
      {
        Data.erase(maingrid.SelectedIndex);
        refresh();
        return;
      }
      if (((Person)(maingrid.SelectedItem)).left > 0)
      {
        ((Person)(maingrid.SelectedItem)).drank++;
        ((Person)(maingrid.SelectedItem)).left--;
        ((Person)(maingrid.SelectedItem)).TimeOfLast = DateTime.Now.ToString("hh:mm tt");

        refresh();
      }
      var item = new ListViewItem();
    }

    /// <summary>
    /// Handles a right click on the listview. Untook a drink.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void MaingridMouseRightButtonUp(object sender, MouseButtonEventArgs e)
    {
      if ((maingrid.SelectedIndex < maingrid.Items.Count) && (maingrid.SelectedIndex >= 0))
      {
        if (Data.EDITMODE)
        {
          Engine.OpenWindow(new editWindow(maingrid.SelectedIndex), this);
          return;
        }
        if (Data.DELETEMODE)
        {
          Data.erase(maingrid.SelectedIndex);
          refresh();
          return;
        }
        if (((Person)(maingrid.SelectedItem)).drank > 0)
        {
          ((Person)(maingrid.SelectedItem)).drank--;
          ((Person)(maingrid.SelectedItem)).left++;

          refresh();
        }
      }
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      Engine.WriteData();
      Engine.Log("Program closed.");
      Application.Current.Shutdown();
    }

    //Edit on
    private void ToggleButton_Checked(object sender, RoutedEventArgs e)
    {
      Data.EDITMODE = true;
      if (Data.DELETEMODE)
      {
        dbutton.IsChecked = false;
        Data.DELETEMODE = false;
      }
    }

    //Edit off
    private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
    {
      Data.EDITMODE = false;
    }

    //Delete on
    private void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
    {
      Data.DELETEMODE = true;
      if (Data.EDITMODE)
      {
        ebutton.IsChecked = false;
        Data.EDITMODE = false;
      }
    }

    //Delete off
    private void ToggleButton_Unchecked_1(object sender, RoutedEventArgs e)
    {
      Data.DELETEMODE = false;
    }

    //Save button 
    private void SaveButtonClick(object sender, RoutedEventArgs e)
    {
      Engine.WriteData();
    }

    private void ExitButtonClick(object sender, RoutedEventArgs e)
    {
      Close();
    }

    //Delete all click
    private void MenuItem_Click(object sender, RoutedEventArgs e)
    {
      for (var i = 0; i <= Data.getData().Count; ++i)
      {
        Data.erase(i);
      }
      refresh();
    }
  }
}
