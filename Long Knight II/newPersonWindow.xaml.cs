﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Documents;

namespace Long_Knight_II
{
    /// <summary>
    /// Interaction logic for newPersonWindow.xaml
    /// </summary>
    public partial class newPersonWindow : Window
    {
        /// <summary>
        /// Initializes the window components.
        /// </summary>
        public newPersonWindow()
        {
            InitializeComponent();
            comboLoad();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Owner.IsEnabled = true;
            Owner.Activate();
        }

        private void Button_Click(object sender, RoutedEventArgs e) //submit
        {
            try
            {
                Data.Add(new Person(nameBox.Text, dropdown.SelectedItem.ToString(), int.Parse(numberbox.Text)));
            }
            catch (Exception ee)
            {
                Engine.Log(ee.Message);
                return;
            }
            Data.fireDataChange();
            Close();
        }

        //Edit Button
        private void editButton_Click(object sender, RoutedEventArgs e)
        {
            Window drinks = new drinkMenuWindow();
            Engine.OpenWindow(drinks, this);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e) //cancel, automatically calls window_closed
        {
            Close();
        }

        private void comboLoad()
        {
            dropdown.Items.Clear();
            string[] str = Engine.ReadDrinks().Split('\n');
            for (int i = 0; i < str.Length-1; ++i )
            {
                dropdown.Items.Add(str[i]);
            }
        }

        private void Window_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e) //When it gains or loses focus, so it reloads combobox
        {
            comboLoad();
            dropdown.SelectedIndex = 0;
        }

    }
}
