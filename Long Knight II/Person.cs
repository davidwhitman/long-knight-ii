﻿using System.Windows.Controls;
using System.Runtime.Serialization;
using System;

/// <summary>
/// Represents a person at a social event, what beverage they have, and how many they have (left and drank).
/// </summary>
[Serializable()]
public class Person : ISerializable
{
    /// <summary>
    /// The name of the Person.
    /// </summary>
    public string name { get; set; }
    /// <summary>
    /// The type of drink the Person brought.
    /// </summary>
    public string type { get; set; }
    /// <summary>
    /// The number of drinks the Person has left.
    /// </summary>
    public int left { get; set; }
    /// <summary>
    /// The number of drinks the Person has taken.
    /// </summary>
    public int drank { get; set; }
    /// <summary>
    /// The time the Person took their last drink.
    /// </summary>
    public string TimeOfLast { get; set; }

    /// <summary>
    /// Default constructor sets name and type to "default" and left and drank to zero.
    /// </summary>
    public Person()
    {
        name = "default";
        type = "default";
        left = 0;
        drank = 0;
        TimeOfLast = " ";
    }

    /// <summary>
    /// Calls default, then modifies name, type, and left to the passed in values.
    /// </summary>
    /// <param name="n">Name of Person</param>
    /// <param name="t">Type of drink</param>
    /// <param name="l">Number left</param>
    public Person(string n, string t, int l)
        : this()
    {
        name = n;
        type = t;
        left = l;
    }

    /// <summary>
    /// Calls default, then modifies name, type, drank, and left to the passed in values.
    /// </summary>
    /// <param name="n">Name of Person</param>
    /// <param name="t">Type of drink</param>
    /// <param name="l">Number left</param>
    /// <param name="d">Number drank</param>
    public Person(string n, string t, int l, int d)
        : this(n, t, l)
    {
        drank = d;
    }

    /// <summary>
    /// Changes the values in Person to new values.
    /// </summary>
    /// <param name="n">New value for Name.</param>
    /// <param name="t">New value for Time.</param>
    /// <param name="l">New value for Left.</param>
    /// <param name="d">New value for Drank.</param>
    public void edit(string n, string t, int l, int d)
    {
        name = n;
        type = t;
        left = l;
        drank = d;
    }

    /// <summary>
    /// Constructor for serialization read.
    /// </summary>
    /// <param name="info"></param>
    /// <param name="ctxt"></param>
    public Person(SerializationInfo info, StreamingContext ctxt)
    {
        name = (string)info.GetValue("PersonName", typeof(string));
        type = (string)info.GetValue("PersonType", typeof(string));
        left = (int)info.GetValue("PersonLeft", typeof(int));
        drank = (int)info.GetValue("PersonDrank", typeof(int));
        TimeOfLast = (string)info.GetValue("PersonTime", typeof(string));
    }



    public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
    {
        info.AddValue("PersonName", name);
        info.AddValue("PersonType", type);
        info.AddValue("PersonLeft", left);
        info.AddValue("PersonDrank", drank);
        info.AddValue("PersonTime", TimeOfLast);
    }

    /// <summary>
    /// Returns a string representation of a Person.
    /// </summary>
    /// <returns>String representation of the Person.</returns>
    public override string ToString()
    {
        return "Name: " + name + " Type: " + type + " Left: " + left + " Drank: " + drank;
    }
}